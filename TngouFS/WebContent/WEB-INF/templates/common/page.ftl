  
  <#if page??>
  
  <ul class="am-pagination am-pagination-centered">
  

<#if page.totalpage &gt; 1> 
	<#assign x=1>
	<#assign mx=page.totalpage>
	
	<#if page.page &gt; 5>
		<#assign x=page.page-3>
	</#if>
	<#assign mx=page.page+5>
	<#if mx &gt;page.totalpage >
		<#assign mx=page.totalpage>
	</#if>
	
	<#if page.page!=1>
	  
   <li ><a href="?p=${page.page-1}&">&laquo;</a></li>
    </#if>
    <#if page.page &gt; 5>
   <li ><a href="?p=1&">1</a></li>
  </#if>
  
  <#list x..mx as i>
	<#if i==page.page><li class="am-active"> <a href="#"  >${i} </a></li><#else>	
	 <li><a href="?p=${i}&">${i}</a></li></#if>
	
	</#list>
  <#if mx &lt; page.totalpage>
     <li><a href="?p=${page.totalpage}&">${page.totalpage}</a></li>
</#if>
  <#if page.page!=page.totalpage>
  <li><a href="?p=${page.page+1}&">&raquo;</a></li>
  </#if>
 </#if>  
  
   </ul>
   
   </#if>