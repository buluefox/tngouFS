<#include "header.ftl">

  <div class="admin-content">

    <div class="am-cf am-padding">
    
      <ol class="am-breadcrumb">
  <li><a href="${Domain.manage}">首页</a></li>
  <li><a href="${Domain.manage}/classify">图库分类</a></li>
  <li class="am-active">列表</li>
</ol>
      
    </div>

    <div class="am-g">
      <div class="am-u-sm-12 am-u-md-6">
        <div class="am-btn-toolbar">
          <div class="am-btn-group am-btn-group-xs">
          <a href="${Domain.manage}/classify/add">
            <button type="button" class="am-btn am-btn-default"><span class="am-icon-plus"></span> 新增</button>
			</a>
          </div>
        </div>
      </div>
  
   
    </div>

    <div class="am-g">
      <div class="am-u-sm-12">

          <table class="am-table am-table-striped am-table-hover table-main">
            <thead>
              <tr>
                <th >排序</th>
                <th >名称</th>
                <th >标题</th>
                <th >关键词</th>
                <th >描述</th>
                <th >操作</th>
              </tr>
          </thead>
          <tbody>
          
           <#list galleryclasses as item>
            <tr>
           
              <td>${item.seq}</td>
              <td><a href="${Domain.manage}/gallery/list/${item.id}">${item.name}</a></td>
              <td>${item.title}</td>
              <td>${item.keywords}</td>
              <td>${item.description}</td>
              <td>
                <div class="am-btn-toolbar">
                  <div class="am-btn-group am-btn-group-xs">
                 
                  <a href="${Domain.manage}/classify/edit?id=${item.id}">
                    <button class="am-btn am-btn-default am-btn-xs am-text-secondary"><span class="am-icon-pencil-square-o"></span> 编辑</button>   
                    </a>    
                  </div>
                </div>
              </td>
            </tr>
             </#list>
          </tbody>
        </table>
  
        

      </div>

    </div>
  </div>
<#include "footer.ftl">
