<#include "header.ftl">

  <!-- content start -->
  <div class="admin-content">

    <div class="am-cf am-padding">


  <ol class="am-breadcrumb">
  <li><a href="${Domain.manage}">首页</a></li>
  <li><a href="${Domain.manage}/gallery/list/${galleryclass.id}">${galleryclass.name}</a></li>
 <li><a href="${Domain.manage}/gallery/show/${gallery.id}">${gallery.title}</a></li>

  <li class="am-active">编辑</li>
</ol>
  </div>

  <hr>
  
<div class="am-g">

    

 
       

     
     <hr>
        <form class="am-form am-form-horizontal" id="form_update"   action="${Domain.manage}/gallery/edit" method="post">
         
          <input name="id" value="${gallery.id}" type="hidden">
          <input name="size" value="${gallery.size}" type="hidden">
           <div class="am-form-group">
		      <label >图库分类</label>
		      <select  name="galleryclass">
		      	<#list galleryclasses as item>
		      	<option  <#if item.id==gallery.galleryclass> selected = "selected"</#if> value="${item.id}">${item.name}</option>
            
            </#list>
		        
		     
		      </select>
		      <span class="am-form-caret"></span>
		    </div>
         
          <div class="am-form-group">
            <label  >标题</label>
            <div >
              <input id="title" name="title" placeholder="图库标题" type="text" value="${gallery.title}">
             
            </div>
          </div>


	<hr>

    <ul class="am-avg-sm-1 am-avg-md-3 am-avg-lg-5 am-margin gallery-list">
    
    <#list list as item>
      <li >
<#if item_index!=0>
       <a href="javascript:deletePicture('${item.id}');"   title="删除" ><i class="am-close  am-close-spin ">&times;</i></a>
</#if>
        <a href="${Domain.imgurl}/img${item.src}" target="_blank">
          <img class="am-img-thumbnail am-img-bdrs" src="${Domain.imgurl}/img${item.src}" alt="">
        
        </a>
        
        
      </li>
      </#list>
    </ul>
          
          
          
          <div class="am-form-group">
            <div class="am-u-sm-9 am-u-sm-push-3">
              <button type="submit" class="am-btn am-btn-primary" name="sub" value="提交">修改图片</button>
            </div>
          </div>
        
        </form>

    

  </div>
  <!-- content end -->


<div class="am-modal am-modal-confirm" tabindex="-1" id="my-confirm-Picture">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><i class="am-icon-question"></i>-天狗提示</div>
    <div class="am-modal-bd">
      你，确定要删除该图片吗？删除不能恢复，请慎重选择！
    </div>
    <div class="am-modal-footer">
      <span class="am-modal-btn" data-am-modal-cancel>取消</span>
      <span class="am-modal-btn" data-am-modal-confirm>确定</span>
    </div>
  </div>
</div>

<script type="text/javascript">

function deletePicture(vid)
{

 	$('#my-confirm-Picture').modal({

		//确定
	 	onConfirm: function(options) {
           
          	$.get("${Domain.manage}/gallery/deletepicture?id="+vid,
				function(html){
						
						location.href="${Domain.manage}/gallery/edit?id="+${gallery.id};
					
				});
        },
        //否
        onCancel: function() {
         
        }
	 });
	

}

</script>


<#include "footer.ftl">
