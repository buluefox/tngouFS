<#include "header.ftl">

  <div class="admin-content">

    <div class="am-cf am-padding">
  <ol class="am-breadcrumb">
  <li><a href="${Domain.manage}">首页</a></li>
  <li><a href="${Domain.manage}/gallery/list/${galleryclass.id}">${galleryclass.name}</a></li>
 <li><a href="${Domain.manage}/gallery/show/${gallery.id}">${gallery.title}</a></li>

  <li class="am-active">详情</li>
</ol>

    </div>
    
    <h2>
    ${gallery.title}
    </h2>
<hr>

    <ul class="am-avg-sm-1 am-avg-md-3 am-avg-lg-5 am-margin gallery-list">
    
    <#list list as item>
      <li>

        <a href="${Domain.imgurl}/img${item.src}" target="_blank">
          <img class="am-img-thumbnail am-img-bdrs" src="${Domain.imgurl}/img${item.src}" alt="">
        
        </a>
       
      </li>
      </#list>
    </ul>

    <div class="am-margin am-cf">
      <hr>
     
    

    
    </div>

  </div>

<#include "footer.ftl">
